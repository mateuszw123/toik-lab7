import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.scene.control.Cell;

import java.io.FileOutputStream;
import java.util.Date;

public class Main {
    private static String FILE = "F:/Test/PDF.pdf";
    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
            Font.BOLD);
    private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.NORMAL, BaseColor.RED);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
            Font.BOLD);
    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.BOLD);

    public static void main(String[] args) {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();


            addContent(document);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private static void addTitlePage(Document document)
            throws DocumentException {
        Paragraph preface = new Paragraph();
        document.add(preface);
        document.newPage();
    }
    private static void addContent(Document document) throws DocumentException {
        Paragraph title = new Paragraph("Resume", catFont);
        title.setAlignment(Element.ALIGN_CENTER);
        document.add(title);
        createTable(document);


    }

    private static void createTable(Document document)
            throws DocumentException {
        PdfPTable table = new PdfPTable(2);
        table.setWidths(new int[]{3, 3});
        table.addCell("First name");
        table.addCell("Mateusz");
        table.addCell("Last name");
        table.addCell("Wielblad");
        table.addCell("education");
        table.addCell("2018 - 2021 PWSZ Tarnów");
        table.addCell("Summary");
        table.addCell("                                         ");
        document.add(table);
    }

}